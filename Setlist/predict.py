import sys
import sklearn
import string
import csv
import numpy as np
import pandas as pd
import warnings
from sklearn.svm import SVC

from sklearn.linear_model import LogisticRegression
#from sklearn.ensemble import AdaBoostClassifier

def create_songs_stat(df):
  #function creates dataframe with additional statistics
  result_df = df.copy()
  result_df['SONGS_ON_CONCERT'] = np.sum(result_df.values, axis = 1)
  
  #print np.sum(result_df.values, axis = 0)
  total_played_df = pd.DataFrame([np.sum(result_df.values, axis = 0)], index = ['TOTAL_PLAYED'], columns = result_df.columns)
  result_df = pd.concat([result_df, total_played_df], axis = 0)
  return result_df

def delete_zeros(df):
  result_df = df.copy()
  stat_df = create_songs_stat(df)
  #delete songs that do not appear
  for col in df.columns:
    if stat_df.loc['TOTAL_PLAYED', col] == 0:
      result_df = result_df.drop(col, axis = 1)
  #delete concerts with empty setlists
  for day in df.index:
    if stat_df.loc[day, 'SONGS_ON_CONCERT'] == 0:
      result_df = result_df.drop(day, axis = 0)
  return result_df

def create_lags_df(df, history):
  #creates df in which each row contains data for current day, day-1, ... day-history+1
  #result contains history-1 rows less (from row history-1 to the last row of df)
  result_df = df.copy()
  #create df that will not be changed
  noshift_df = df.copy()
  #for first history-1 days lags are unknown, so first of all we drop them
  result_df = result_df.iloc[history-1:]
  #add lags
  for i in range(1, history):
    #prepare list of columns
    temp_col_l = noshift_df.columns.tolist()
    for j in range(len(temp_col_l)):
      temp_col_l[j] = temp_col_l[j] + '_lag' + str(i)
    #concatenate current lag dataframe with result_df
    temp_df = noshift_df.iloc[history-1-i:-i].copy()
    temp_df.columns = temp_col_l
    temp_df.index = result_df.index  #to concatenate row to row, not day to day
    result_df = pd.concat([result_df, temp_df], axis = 1)
  
  return result_df

def predict(df, n_day = 'last', history = 1, setlist_length = 'auto', test_flag = True):
  #train classifier using days 1 to n_day-1
  #predict on day number n_day
  #n_day can have 2 special values: 'last' and 'next'
  if n_day == 'last':#predict last known setlist
    n_day = len(df) - 1
  if n_day == 'next':#predict next concert's setlist
    n_day = len(df)
  
  if history == 'auto':
    h_max = len(df) - 2
    err_min = 1000
    h_best = 1
    h = 1
    while h < h_max:
      pred_h_df = predict(df, n_day = 'last', history = h, test_flag = False)
      n00, n11, n01, n10 = compare_pred_test(pred_h_df.loc[:,'exist'].values.tolist(), df.iloc[n_day,:].values.tolist(), show = False)
      err = n01 + n10
      if err < err_min:
        h_best = h
        err_min = err
      #else:
      #  break
      h += 1
    if n_day == 'next':
      history = h + 1
    else:
      history = h
      
  #create supporting df's; remember that i-th row of lags_df corresponds to i-(history-1)-th row of df
  lags_df = create_lags_df(df, history)
  n_day_lags_df = n_day - (history - 1)#number of day to predict in lags_df
  pred_df = pd.DataFrame(np.zeros((len(df.columns), 2)), columns = ['proba', 'exist'], index = df.columns)
  if test_flag:
    print lags_df
    print "n_day_lags_df: %d" % (n_day_lags_df)
    print "number of rows in lags_df: %d" % (len(lags_df))
    lags_df.to_excel('lags.xls')

  #train classifier and predict probabilities; x_df and y_df - training data
  x_df = lags_df.iloc[ : n_day_lags_df - 1].copy()
  y_df = df.iloc[history : n_day].copy()
  if test_flag:
    print "number of rows in x_df: %d" % (len(x_df))
    print "number of rows in y_df: %d" % (len(y_df))
  for song in df.columns:
    if test_flag: print song
    clf = LogisticRegression(penalty = 'l1')
    #clf = AdaBoostClassifier(n_estimators = 80)
    #clf = SVC(kernel = 'rbf', probability = True)
    if (all(y_df[song][:] == 1) or all(y_df[song][:] == 0)):
      #trivial case
      if all(y_df[song][:] == 1):
        pred_df.loc[song, 'proba'] = 1
        if test_flag: print '%0.3f' % 1
      if all(y_df[song][:] == 0):
        pred_df.loc[song, 'proba'] = 0
        if test_flag: print '%0.3f' % 0
    else:
      clf.fit(x_df.values, y_df[song].values)
      pred_df.loc[song, 'proba'] = clf.predict_proba(lags_df.values[n_day_lags_df-1,:])[0,1]
      if test_flag: print '%0.3f' % pred_df.loc[song, 'proba']
  
  #predict labels by sorting probabilities
  if setlist_length == 'auto':
    if n_day < len(df):
      #we are trying to predict known setlist
      #do not predict length - get it from data
      setlist_length = np.sum(df.iloc[n_day,:].values, axis = 0)
      if test_flag: print 'length of predicted setlist: %d' % setlist_length
    else:
      #we are predicting unknown setlist
      #!!!HERE WILL BE A PREDICTOR (to be done)
      #now it just takes length from previous concert
      setlist_length = np.sum(df.iloc[-1,:].values, axis = 0)
  pred_df.sort(columns = 'proba', ascending = False, inplace = True)
  pred_df['exist'][:setlist_length] = 1
  #sort by songnames again. Essential for correct testing!
  pred_df.sort(axis = 0, inplace = True)
  return pred_df

def find_best_history(df):
  n_day = len(df) - 1
  h_max = min(len(df) - 2, len(df) // 2)
  err_min = 1000
  h_best = 1
  h = 1
  while h < h_max:
    pred_h_df = predict(df, n_day = 'last', history = h, test_flag = False)
    n00, n11, n01, n10 = compare_pred_test(pred_h_df.loc[:,'exist'].values.tolist(), df.iloc[n_day,:].values.tolist(), show = False)
    err = n01 + n10
    if err < err_min:
      h_best = h
      err_min = err
    h += 1
    
  return h_best
  
def predict_naive(df, n_day, setlist_length = 'auto', test_flag = True):
  #naive prediction: most popular songs are in setlist
  #predict on day number n_day
  #n_day can have 2 special values: 'last' and 'next'
  if n_day == 'last':#predict last known setlist
    n_day = len(df) - 1
  if n_day == 'next':#predict next concert's setlist
    n_day = len(df)
    
  #define number of songs in setlist
  if setlist_length == 'auto':
    if n_day < len(df):
      #we are trying to predict known setlist
      #do not predict length - get it from data
      setlist_length = np.sum(df.iloc[n_day,:].values, axis = 0)
      if test_flag: print 'length of predicted setlist: %d' % setlist_length
    else:
      #we are predicting unknown setlist
      #!!!HERE WILL BE A PREDICTOR (to be done)
      #now it just takes length from previous concert
      setlist_length = np.sum(df.iloc[-1,:].values, axis = 0)
  
  stat_df = create_songs_stat(df.iloc[:n_day,:])
  pred_naive_df = pd.DataFrame(np.zeros((len(df.columns), 2)), index = df.columns, columns = ['proba_naive', 'exist_naive'])
  max_total = float(max(stat_df.loc['TOTAL_PLAYED',:][:-1].tolist()))
  
  for song in df.columns:
    pred_naive_df.loc[song, 'proba_naive'] = stat_df.loc['TOTAL_PLAYED', song] / max_total
  
  pred_naive_df.sort(columns = 'proba_naive', ascending = False, inplace = True)
  pred_naive_df['exist_naive'][:setlist_length] = 1
  #sort by songnames again. Essential for correct testing!
  pred_naive_df.sort(axis = 0, inplace = True)
  return pred_naive_df

def compare_pred_test(pred_l, test_l, show = True):
    #function prints results of comparing prediction and real data
    #and returns numbers of PNF, PF, MF, FA
    #pred_l and test_l - predicted labels and test (real) labels
    
    n00, n01, n10, n11 = 0,0,0,0
    if len(pred_l) != len(test_l):
        print "Lengths are different"
        return 0
    
    for i in range(len(pred_l)):
        if pred_l[i] == 0 and test_l[i] == 0: n00+=1
        if pred_l[i] == 1 and test_l[i] == 0: n10+=1
        if pred_l[i] == 0 and test_l[i] == 1: n01+=1
        if pred_l[i] == 1 and test_l[i] == 1: n11+=1
    #RNE - Right Not Exist, RE - Right Exist, WNE - Wrong Not Exist, WE - Wrong Exist
    if show: print "  RNE: %d; RE: %d; WNE: %d; WE: %d" %(n00,n11,n01,n10)
    return [n00, n11, n01, n10]
    
def main():
  warnings.filterwarnings("ignore", category=DeprecationWarning)
  
  group_name = sys.argv[1]
  predict_mode = sys.argv[2]#can be 'last' or 'next'
  ###initialization of parameters
  history = 'auto'
  global_test_flag = False
  
  ###preparation of data
  filename = './data/' + group_name + '.csv'
  songs_raw_df = pd.read_csv(filename, sep = ';', index_col = 0)
  #print songs_raw_df.shape
  songs_stat_df = create_songs_stat(songs_raw_df)
  #print songs_stat_df.shape
  songs_df = delete_zeros(songs_raw_df)
  
  #n_day can be also an exact number, but it is not implemented in the interface
  if predict_mode == 'last':#predict last known setlist
    n_day = len(songs_df) - 1
  if predict_mode == 'next':#predict next concert's setlist
    n_day = len(songs_df)
  
  ###conducting prediction
  if history == 'auto':
    history = find_best_history(songs_df)
    print "  Best history parameter: %d" % (history)
  pred_df = predict(songs_df, n_day = n_day, history = history, test_flag = global_test_flag)
  
  if predict_mode == 'last':
    ###Prediction on last known concert and cross-validation results
    print '  Prediction results:'
    print pred_df.iloc[:min(25, len(pred_df)-1),:].sort(columns = 'proba', ascending = False, inplace = False)
    pred_naive_df = predict_naive(songs_df, n_day = n_day, test_flag = global_test_flag)
    #print pred_naive_df
    print '  Logistic Regression (history = %d) results:' % history
    compare_pred_test(pred_df.loc[:,'exist'].values.tolist(), songs_df.iloc[n_day,:].values.tolist())
    print '  Naive Predictor results:'
    compare_pred_test(pred_naive_df.loc[:,'exist_naive'].values.tolist(), songs_df.iloc[n_day,:].values.tolist())
  
  if predict_mode == 'next':
    ###Prediction on next concert
    print '  Prediction results:'
    print pred_df.iloc[:min(25, len(pred_df)-1),:].sort(columns = 'proba', ascending = False, inplace = False)
    
  
  #pred_df.to_excel('tst.xls')
  
if __name__ == '__main__':
  main()
