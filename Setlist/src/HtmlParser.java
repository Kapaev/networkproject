import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlParser {

	public static String getArtistLink(String name) throws IOException {

		Document doc;
		String artistLink = null;
		int i = 1;
		do {

			String html = "http://www.setlist.fm/artist/browse/".concat(
					name.substring(0, 1)).toLowerCase();

			// System.out.println(html.concat("/1.html"));
			doc = Jsoup.connect(html + "/" + Integer.toString(i) + ".html")
					.get();
			Elements links = doc.select("a[href]");

			for (int j = links.size() - 1; j >= 0; --j) {

				Element link = links.get(j);
				// System.out.println(name);
				if (link.text().startsWith(name)) {
					// System.out.println(link.text());
					artistLink = link.attr("href");
					// System.out.println(artistLink.substring(artistLink.lastIndexOf("/")));
					artistLink = artistLink.substring(artistLink
							.lastIndexOf("/"));
				}
			}
			++i;
		} while (artistLink == null);

		return artistLink;
	}

	private static String date = null;

	public static ArrayList<Song> getAllSongs(String artistLink) {

		ArrayList<Song> songs = new ArrayList<Song>();
		Document doc;
		try {

			System.out.println(artistLink);
			doc = Jsoup.connect(
					"http://www.setlist.fm/stats/songs".concat(artistLink))
					.get();

			Elements links = doc.select("a[href]");
			for (Element link : links) {

				if (link.attr("href").contains("../stats/songs")) {

					// System.out.println("\nlink : " + link.attr("href"));
					songs.add(new Song(link.text()));
					String text = link.text();
					// System.out.print(text+" ");
				}
				if (link.attr("href").contains("../search?query=song")) {

					songs.get((songs.size() - 1)).setCount(
							Integer.parseInt(link.text()));
					// System.out.println("\nlink : " + link.attr("href"));
					String text = link.text();
					// System.out.println(text);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return songs;
	}

	public static ArrayList<Concert> getAllConcerts(String artistLink,
			String artist) throws IOException {

		Document doc;
		ArrayList<Concert> concerts = new ArrayList<Concert>();

		for (int i = 10; i > 0; --i) {
			try {
				System.out.println("http://www.setlist.fm/setlists".concat(
						artistLink)
						.concat("?page=".concat(Integer.toString(i))));
				doc = Jsoup.connect(
						"http://www.setlist.fm/setlists".concat(artistLink)
								.concat("?page=".concat(Integer.toString(i))))
						.get();
				Elements links = doc.select("a[href]");

				for (int j = links.size() - 1; j >= 0; --j) {
					Element link = links.get(j);
					if (link.text().contains(artist.concat(" at"))) {
						concerts.add(new Concert("http://www.setlist.fm"
								.concat(link.attr("href").substring(2))));
					}
				}

			} catch (Exception ex) {
				continue;
			}

		}
		return concerts;
	}

	private static int getIndexOfSong(ArrayList<Song> allSongs, String song) {

		for (int i = 0; i < allSongs.size(); ++i) {
			if (allSongs.get(i).getName().equals(song))
				return i;
		}

		return -1;
	}

	public static int getlistSize(int[] setlist) {

		int size = 0;
		for (int i = 0; i < setlist.length; ++i) {
			if (setlist[i] == 1)
				size += 1;
		}
		return size;
	}

	public static int[] getSetList(String concertLink, ArrayList<Song> allSongs)
			throws IOException {
		Document doc;
		doc = Jsoup.connect(concertLink).get();

		Elements links = doc.select("a[href]");
		int[] setlist = new int[allSongs.size()];
		for (int i = 0; i < setlist.length; ++i) {
			setlist[i] = 0;
		}

		Elements divs = doc.select("div[class]");
		for (Element div : divs) {
			if (div.hasClass("DateBlock")) {
				date = div.text();
			}

		}
		for (Element link : links) {

			if (link.attr("href").contains("../stats/songs")) {
				String songName = link.text();
				int index = getIndexOfSong(allSongs, songName);
				if (index >= 0)
					setlist[index] = 1;
			}
		}

		return setlist;
	}

	public static String getDate() {
		return date;
	}

}