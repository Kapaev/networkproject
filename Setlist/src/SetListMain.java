import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class SetListMain {

	private static void downloadInfo(String artist) throws IOException {

		String artistLink = HtmlParser.getArtistLink(artist);
		File flt = new File("./data/".concat(artist.concat(".csv")));
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(flt)));

		ArrayList<Song> allSongs = HtmlParser.getAllSongs(artistLink);

		for (Song song : allSongs)
			System.out.println(song.getName());

		ArrayList<Concert> recentConcerts = HtmlParser.getAllConcerts(
				artistLink, artist);
		int[] setlist = new int[allSongs.size()];
		out.print("Date;" + 0);
		for (int i = 1; i < allSongs.size(); ++i) {
			out.print(";" + allSongs.get(i).getName());
		}
		out.println();
		String date = HtmlParser.getDate();
		for (Concert concert : recentConcerts) {
			// System.out.println(concert.getLink());
			setlist = HtmlParser.getSetList(concert.getLink(), allSongs);

			if (date != null)
				if (date.equals(HtmlParser.getDate())) {
					System.out.println("!\n");
					out.print("!!");
				}
			date = HtmlParser.getDate();
			out.print(date);

			for (int i = 0; i < setlist.length; ++i) {
				out.print(";" + setlist[i]);
			}
			out.println();
			out.flush();
		}
		Process p = Runtime.getRuntime().exec(
				"python predict.py " + artist + "last");
		Scanner pythonIn = new Scanner(p.getInputStream());
		while (pythonIn.hasNext())
			System.out.println(pythonIn.nextLine());

		pythonIn.close();
	}

	public static void main(String[] args) throws IOException {

		Scanner in = new Scanner(System.in);
		String artist = in.nextLine();
		System.out.println(artist);

		downloadInfo(artist);
		Process p = Runtime.getRuntime().exec(
				"python predict.py " + artist+ "next");
		Scanner pythonIn = new Scanner(p.getInputStream());
		while (pythonIn.hasNext())
			System.out.println(pythonIn.nextLine());

		pythonIn.close();
		// new GuiApp1();

	}

}