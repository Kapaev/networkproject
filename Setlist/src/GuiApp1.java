import java.awt.BorderLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class GuiApp1 {
	private JTextField textField;
	private JTextField textField_1;

	// Note: Typically the main method will be in a
	// separate class. As this is a simple one class
	// example it's all in the one class.

	public GuiApp1() {
		JFrame guiFrame = new JFrame();

		// make sure the program exits when the frame closes
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("Example GUI");
		guiFrame.setSize(627, 433);

		// This will center the JFrame in the middle of the screen
		guiFrame.setLocationRelativeTo(null);

		// Options for the JComboBox
		String[] fruitOptions = { "Apple", "Apricot", "Banana", "Cherry",
				"Date", "Kiwi", "Orange", "Pear", "Strawberry" };

		// Options for the JList
		String[] vegOptions = { "Asparagus", "Beans", "Broccoli", "Cabbage",
				"Carrot", "Celery", "Cucumber", "Leek", "Mushroom", "Pepper",
				"Radish", "Shallot", "Spinach", "Swede", "Turnip" };
		guiFrame.getContentPane().setLayout(new BorderLayout(0, 0));

		// The first JPanel contains a JLabel and JCombobox
		final JPanel comboPanel = new JPanel();
		comboPanel.setLayout(null);
		JLabel comboLbl = new JLabel("Enter group name");
		comboLbl.setBounds(5, 7, 127, 15);

		comboPanel.add(comboLbl);

		// The JFrame uses the BorderLayout layout manager.
		// Put the two JPanels and JButton in different areas.
		guiFrame.getContentPane().add(comboPanel, BorderLayout.CENTER);
		
		textField = new JTextField();
		textField.setBounds(137, 5, 114, 19);
		comboPanel.add(textField);
		textField.setColumns(10);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Predict next setlist");
		rdbtnNewRadioButton.setBounds(5, 29, 159, 23);
		rdbtnNewRadioButton.setHorizontalAlignment(SwingConstants.LEFT);
		comboPanel.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Test on last known setlist");
		rdbtnNewRadioButton_1.setBounds(5, 57, 209, 23);
		rdbtnNewRadioButton_1.setHorizontalAlignment(SwingConstants.LEFT);
		comboPanel.add(rdbtnNewRadioButton_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(282, 5, 284, 357);
		comboPanel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Start");
		btnNewButton.setBounds(15, 88, 117, 25);
		comboPanel.add(btnNewButton);
		
		ButtonGroup myGroup = new ButtonGroup();
		myGroup.add(rdbtnNewRadioButton_1);
		myGroup.add(rdbtnNewRadioButton);

	    

		// make sure the JFrame is visible
		guiFrame.setVisible(true);
	}
}
