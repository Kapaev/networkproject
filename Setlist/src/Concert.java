import java.util.ArrayList;

public class Concert {

	private String link;
	private int allCount;
	private ArrayList<Song> songs;

	public Concert(String link) {
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public int getCount() {
		return allCount;
	}

	public void setCount(int count) {
		this.allCount = count;
	}
}