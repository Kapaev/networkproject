public class Song{
	
	private String name;
	private int allCount;
	
	public Song(String name){
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	
	public int getCount(){
		return allCount;
	}
	
	public void setCount(int count){
		this.allCount = count; 
	}
}